import requests
import json
import time
from secrets import token


tag = "yeezy"
url = "https://api.digitalocean.com/v2/droplets"
tag_url = "https://api.digitalocean.com/v2/droplets?tag_name=" + tag + ""
# key_url ="https://api.digitalocean.com/v2/account/keys"
f = open("do_proxy.sh", "r")
user_data_string = f.read()
headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + token + ""
            }
payload = {
            "names": [
                "yeezy1",
                "yeezy2"
              ],
              "region": "sfo1",
              "size": "512mb",
              "image": "centos-6-x64",
              "ssh_keys": [
                  "15768305"
                  ],
              "user_data": user_data_string,
              "tags": [
                "yeezy"
              ]
            }


def create_droplets():
    # This HTTP request creates multiple droplets
    post_req = requests.post(url, data=json.dumps(payload), headers=headers)
    print json.dumps(post_req.json(), indent=4, sort_keys=True)
    time.sleep(5)

    get_req = requests.get(url, headers=headers)
    output = json.loads(get_req.text)
    amount_of_droplets = len(output['droplets'])

    print "IP ADDRESS"
    for x in xrange(0, amount_of_droplets):
        print output['droplets'][x]['networks']['v4'][0]['ip_address']


def get_droplet_info():
    # This HTTP request gets information on all droplets
    req = requests.get(url, headers=headers)
    output = json.dumps(req.json(), indent=4, sort_keys=True)
    print "Here are all the droplet details: "
    print output


def destroy_droplets():
    # This HTTP request deletes all droplets with the "tag"
    req = requests.delete(tag_url, headers=headers)
    print "Destroyed all droplets with the tag " + "\"" + tag + "\""


def main():
    while True:
        try:
            print "Please select a choice: \n" \
                  "1. Create multiple droplets \n" \
                  "2. Get droplet information \n" \
                  "3. Destroy droplets \n" \
                  "4. Exit \n"

            choice = int(raw_input("Enter your choice: "))
        except ValueError:
            print("ERROR")
            continue
        else:
            break

    if choice == 1:
        # print "create_droplets"
        create_droplets()
    elif choice == 2:
        # print "get_droplet_info"
        get_droplet_info()
    elif choice == 3:
        # print "destroy_droplets"
        destroy_droplets()
    elif choice == 4:
        pass

    # create_droplets()
    # get_droplet_info()
    # destroy_droplets()


if __name__ == "__main__":
    main()




########################################################################################################
# OLD CODE BELOW

# This HTTP request gets information on all droplets
# req = requests.get(url, headers=headers)

# This HTTP request creates multiple droplets
# req = requests.post(url, data=json.dumps(payload), headers=headers)

# This HTTP request deletes all droplets with the "tag"
# req = requests.delete(tag_url, headers=headers)

# output = json.dumps(req.json(), indent=4, sort_keys=True)
# print output
# print json.dumps(r.json(), indent=4, sort_keys=True)

# output = json.loads(req.text)
#
# amount_of_droplets = len(output['droplets'])
#
# for x in xrange(0, amount_of_droplets):
#     print output['droplets'][x]['networks']['v4'][0]['ip_address']